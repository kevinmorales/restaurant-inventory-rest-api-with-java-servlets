/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

CREATE DATABASE inventory;
CREATE TABLE `march` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `unit` varchar(30) NOT NULL,
  `unitCost` double NOT NULL,
  `amountOnHand` double NOT NULL,
  `totalCost` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `march` (`id`, `name`, `unit`, `unitCost`, `amountOnHand`, `totalCost`) VALUES
(1, 'ribeye', 'lb', 10.99, 3.2, 35.17),
(2, 'ny strip', 'lb', 8.99, 7.2, 64.73),
(3, 'sirloin', 'lb', 8.45, 5.6, 47.32),
(4, 'chicken', 'lb', 6.9, 4.88, 33.67),
(5, 'turkey', 'lb', 9.1, 2.75, 25.025),
(6, 'clams', 'each', 0.99, 85, 108),
(7, 'mussels', 'each', 0.99, 100, 168.48),
(8, 'black beans', '8 oz can', 6, 0.99, 5.93),
(9, 'olive oil', 'gallon', 5, 15.88, 79.4),
(10, 'olives', '16oz jar', 3, 2.75, 8.25),
(11, 'flour', 'lb', 3, 2, 6),
(12, 'salmon', 'lb', 11, 8.8, 96.8),
(13, 'turkey', 'lb', 3.4, 5.76, 19.584),
(14, 'peanuts', 'lb', 2.5, 7.99, 19.975);

CREATE TABLE `february` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `unit` varchar(30) NOT NULL,
  `unitCost` double NOT NULL,
  `amountOnHand` double NOT NULL,
  `totalCost` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `february` (`id`, `name`, `unit`, `unitCost`, `amountOnHand`, `totalCost`) VALUES
(1, 'scallops', 'lb', 45.09, 16.99, 766.0791),
(2, 'walnuts', 'lb', 50.8, 2.99, 151.892),
(3, 'cranberries', 'lb', 50.8, 2.99, 151.892),
(4, 'cashews', 'lb', 4.6, 12.99, 59.754);

CREATE TABLE `january` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `unit` varchar(30) NOT NULL,
  `unitCost` double NOT NULL,
  `amountOnHand` double NOT NULL,
  `totalCost` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `january` (`id`, `name`, `unit`, `unitCost`, `amountOnHand`, `totalCost`) VALUES
(1, 'sirloin', 'lb', 12, 9, 103.39),
(4, 'clams', 'lb', 100, 1, 99),
(5, 'paprika', 'kg', 5, 4, 17.96);

CREATE TABLE `april` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `unit` varchar(30) NOT NULL,
  `unitCost` double NOT NULL,
  `amountOnHand` double NOT NULL,
  `totalCost` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `april` (`id`, `name`, `unit`, `unitCost`, `amountOnHand`, `totalCost`) VALUES
(1, 'paprika', 'kg', 5, 4, 17.96),
(2, 'ribeye', 'lb', 15, 11, 165.07),
(3, 'sirloin', 'lb', 12, 9, 103.39);
