package com.kevin.csc435;

import java.io.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.sql.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@SuppressWarnings("serial")
public class PostInventoryController extends HttpServlet {
    private Gson gson = new GsonBuilder().setPrettyPrinting().create();  
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//local constants
		
		//local variables
		HttpSession session = request.getSession();
		PrintWriter out;
		String item, unit, month;
		double unitCost, amount;
		Product myProduct;
		int idNum;
		
		/***********************/
		
		if (session.getAttribute("month") != null) {
			
			month = (String) session.getAttribute("month");
			item = (String) session.getAttribute("item");
			unit = (String) session.getAttribute("unit");
			unitCost = Double.valueOf((String) session.getAttribute("unitCost"));
			amount = Double.valueOf((String) session.getAttribute("amount"));
			
			myProduct = new Product(item, unit, unitCost, amount);
			
			idNum = AddToDb(myProduct, month);
			
			session.setAttribute("idNum", idNum);
						
			//Set response type
	        response.setContentType("application/json");
	        response.setCharacterEncoding("UTF-8");
	  
	        response.setStatus(HttpServletResponse.SC_OK);
	        ServletContext context = getServletContext();
	        RequestDispatcher rd = context.getRequestDispatcher("/View");
	        rd.forward(request,response);
	        
		}
		else {
			
			response.setStatus(404);
			
		}
		
	}
	
	public int AddToDb(Product x, String month) {
		
		//local constants
		
		//local variables
		Connection con;
		ResultSet rs;
		Statement statement;
		String query;
		int idNum = 0;
		
		/**********************/
		
		try {
	        
	        // Used to issue queries to the DB
			
	        con = DatabaseConnection.initializeDatabase();
	        
	        // Sends queries to the DB for results
	        statement = con.createStatement();
	        
	        // Add a new entry
	        query = "INSERT INTO " + month + " " +
	        "(id, name, unit, unitCost, amountOnHand, totalCost) " + 
	        "VALUES (NULL" + ", '"
	        + x.getItem() + "', '"
	        + x.getUnit() + "'," 
	        + x.getUnitCost() + ", " 
	        + x.getAmount() + ", "
	        + x.getTotalCost() + ")";
	        
	        
	        // Execute the Query
	        statement.executeUpdate(query);
	        
	        statement = con.createStatement();
	        
	        query = "SELECT id FROM " + month  + " WHERE name = " + "'" + x.getItem() + "'";
	        rs = statement.executeQuery(query);
	        
	        while (rs.next())
	        {
	        	idNum = rs.getInt("id");
	        }
	        // Close DB connection
	        con.close();
	        
		} 
		catch (ClassNotFoundException e) {
			
		} 
		catch (SQLException e) {
			
		}
		return idNum;
		
		
	}
	
}
