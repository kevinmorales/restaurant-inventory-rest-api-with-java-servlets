package com.kevin.csc435;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class AboutServlet extends HttpServlet {
        
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        	
    	response.setContentType("text/html;charset=UTF-8");
    	PrintWriter out = response.getWriter();
  
    	try {
    		out.println("<!DOCTYPE html>");
    		out.println("<html><head>");
    		out.println("<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>");
    		out.println("<title>Inventory Management Application</title></head>");
    		out.println("<body>");
    		out.println("<h1>Inventory Management Application</h1>");
    		out.println("<p>This web application will help manage the inventory and food costs of Scotch �N Sirloin,\r\n"
          		+ "a Syracuse steakhouse with thousands of dollars in inventory. According to researcher Dana\r\n"
          		+ "Gunders, �approximately 4 to 10 percent of food purchased by restaurants is wasted before\r\n"
          		+ "reaching the consumer. Drivers of food waste at restaurants include oversized portions,\r\n"
          		+ "inflexibility of chain store management and extensive menu choices�. This food waste is harmful\r\n"
          		+ "to the restaurant industry, so as a solution, this application will help in making calculated\r\n"
          		+ "decisions to manage food inventory and overall food cost of the business. This web application\r\n"
          		+ "will set up Scotch �N Sirloin�s inventory in a database, the inventory manager will be able to\r\n"
          		+ "update this inventory database by adding new items, deleting items on the inventory, updating\r\n"
          		+ "unit costs and amount of product on hand, and viewing the inventory in the web application. The\r\n"
          		+ "application will be able to take the information in the database and calculate total inventory\r\n"
          		+ "costs. The inventory manager will be able to input food sales and purchases and the application\r\n"
          		+ "will be able to calculate food cost percentage for the given month of business. Once this is\r\n"
          		+ "calculated, the food cost percentage will be displayed. To maintain security, a user must log in\r\n"
          		+ "with their credentials to access any of the Scotch �N Sirloin database of inventory. The web\r\n"
          		+ "application will handle authentication of credentials, manage the creation, retrieval, updating,\r\n"
          		+ "and deletion of items in the database, and perform calculations to output food cost percentage.\r\n"
          		+ "Gunders, Dana. �Wasted: How America is Losing Up to 40 Percent of Its Food from Farm to\r\n"
          		+ "Fork to Landfill.� Natural Resources Defense Council, 2017. Retrieved March 7, 2019,\r\n"
          		+ "from https://www.nrdc.org/sites/default/files/wasted-2017-report.pdf</p>");
    		out.println("</body>");
    		out.println("</html>");
    	}
    	finally {
          out.close(); 
       }
    }

}
