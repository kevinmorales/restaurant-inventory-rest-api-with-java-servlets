package com.kevin.csc435;

import java.sql.Connection; 
import java.sql.DriverManager; 
import java.sql.SQLException; 
import java.util.*;
import java.io.*;

  
// This class can be used to initialize the database connection 
public class DatabaseConnection { 
	
    protected static Connection initializeDatabase() throws SQLException, ClassNotFoundException 
    { 
    	//local constants
    	
    	//local variables
    	Connection con;
    	
    	/************************/
        
        // Everything needed to connect to the DB
		Class.forName("com.mysql.cj.jdbc.Driver");
		String url = "jdbc:mysql://localhost/inventory";
		String user = "kevin";
		String pw = "password";
		
		// Used to issue queries to the DB
		con = DriverManager.getConnection(url, user, pw);
        return con; 
    } 
    
    
    
    
} 
