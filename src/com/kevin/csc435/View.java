package com.kevin.csc435;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.google.gson.*;

@SuppressWarnings("serial")

public class View extends HttpServlet {
	
	private HttpSession session;
	private PrintWriter out;
	private String output;
	private Gson gson = new GsonBuilder().setPrettyPrinting().create();  
		
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//local constants
		
		//local variables
		
		/*****************************/
		
		response.setContentType("application/json; charset=UTF-8");

        session = request.getSession();
        out = response.getWriter();

        if (session.getAttribute("getInventoryJSON") != null) {
        	        	
        	output = (String) session.getAttribute("getInventoryJSON");
        	
            out.print(output);
            out.flush();
            
        } 
        else if(session.getAttribute("foodCostJson") != null) {
        	
        	output = (String) session.getAttribute("foodCostJson");
        	
        	out.print(output);
            out.flush();
        	
        }
        else {
        	
            out.print("{\"status\": false}");
            out.flush();
        }
        
        
        session.invalidate();
        out.close();
    }
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//local constants
		
		//local variables
		JsonObject jsonObj;
		int id;
		
		response.setContentType("application/json; charset=UTF-8");

        session = request.getSession();
        out = response.getWriter();
        
        if(session.getAttribute("idNum") != null) {
        	
        	id = (Integer) session.getAttribute("idNum");
        	output = "{\"id\":" + id + "}";
        	out.print(output);
            out.flush();
        }
        
        	
		else {
        	
            out.print("{\"status\": false}");
            out.flush();
        }
        
        
        session.invalidate();
        out.close();
    }

	
	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request,response);
	}
	
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request,response);
	}
	
}


