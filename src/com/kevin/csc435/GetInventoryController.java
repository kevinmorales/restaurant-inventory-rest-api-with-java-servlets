package com.kevin.csc435;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.*;

@SuppressWarnings("serial")
public class GetInventoryController extends HttpServlet {
	
	private Gson gson = new GsonBuilder().setPrettyPrinting().create();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//local constants
		
		//local variables
		String month;
		String id;
		String jsonOutput;
		ArrayList<String> myList;
		HttpSession session;
		JsonObject jsonObj = new JsonObject();
		JsonArray jsonArrayString;
		String[]months = {	"january","february","march", "april"};
		
		/***********************************/
				
		response.setContentType("application/json; charset=UTF-8");

        session = request.getSession();
        
        
        //IF month and id both exist in the session
        if (session.getAttribute("month") != null && session.getAttribute("id") != null) {
        	
        	month = ((String) session.getAttribute("month")).toLowerCase();
        	id = (String) session.getAttribute("id");
        	
        	myList = getFromDB(month, id);
        	
        	jsonArrayString = gson.toJsonTree(myList).getAsJsonArray();
        	jsonObj.add(month, jsonArrayString);

        }
        else if (session.getAttribute("month") != null) {
        	
        	month = ((String) session.getAttribute("month")).toLowerCase();
        	
        	myList = getFromDB(month);
        	
        	jsonArrayString = gson.toJsonTree(myList).getAsJsonArray();
        	jsonObj.add(month, jsonArrayString);

        	
        }
        //ELSE print all months of inventory
        else{
        	
        	for(int i = 0; i < months.length; i++) {
        		
        		myList = getFromDB(months[i]);
        		jsonArrayString = gson.toJsonTree(myList).getAsJsonArray();
            	jsonObj.add(months[i], jsonArrayString);
        		
        	}//END FOR
        	
        }//END IF
        
        jsonOutput = jsonObj.toString();
    	session.setAttribute("getInventoryJSON", jsonOutput);
        ServletContext context = getServletContext();
        RequestDispatcher rd = context.getRequestDispatcher("/View");
        rd.forward(request,response);
		
	
	}//END doGet
	
	
	/************************************************
	 * This function returns all the inventory from a 
	 * given month that is passed as a String in the
	 * parameters of the function. 
	 */
	
	public ArrayList<String> getFromDB(String month){
		
		ArrayList<String> myList = new ArrayList<String>();
		Connection con;
		Statement statement;
		ResultSet rs;
		String query = "SELECT * FROM ";
		
		
		try {
	        
			//Connect to the database
	        con = DatabaseConnection.initializeDatabase();
	        
	        // Sends queries to the DB for results
	        statement = con.createStatement();
	       
	        query += month + ";";
	        
	        rs = statement.executeQuery(query);
	        
	        //iterate through the java result set
	        while (rs.next())
	        {
	          int idNum = rs.getInt("id");
	          String name = rs.getString("name");
	          String unit = rs.getString("unit");
	          double unitCost = rs.getDouble("unitCost");
	          double amountOnHand = rs.getDouble("amountOnHand");
	          double totalCost = rs.getDouble("totalCost");
	          
	          Product myProduct = new Product(idNum,name,unit,unitCost,amountOnHand, totalCost);
	          myList.add(gson.toJson(myProduct));
	     
	        }

	        // Close DB connection
	        con.close();
		} 
		catch (ClassNotFoundException e) {
			e.printStackTrace();
		} 
		catch (SQLException e) {
			e.printStackTrace();
		}
		
		return myList;
	}
	
	public ArrayList<String> getFromDB(String month, String id){
		
		//local constants
		
		//local variables
		ArrayList<String> myList = new ArrayList<String>();
		Connection con;
		Statement statement;
		ResultSet rs;
		String query = "SELECT * FROM ";
		
		/******************************************/
		
		try {
	        
			//Connect to the database
	        con = DatabaseConnection.initializeDatabase();
	        
	        // Sends queries to the DB for results
	        statement = con.createStatement();
	       
	        query += month + " where id = " + id + ";";
	        
	        rs = statement.executeQuery(query);
	        
	        //iterate through the java result set
	        while (rs.next())
	        {
	          int idNum = rs.getInt("id");
	          String name = rs.getString("name");
	          String unit = rs.getString("unit");
	          double unitCost = rs.getDouble("unitCost");
	          double amountOnHand = rs.getDouble("amountOnHand");
	          double totalCost = rs.getDouble("totalCost");
	          
	          Product myProduct = new Product(idNum,name,unit,unitCost,amountOnHand, totalCost);
	          myList.add(gson.toJson(myProduct));
	     
	        }//END WHILE
	        
		} 
		catch (ClassNotFoundException e) {
			e.printStackTrace();
		} 
		catch (SQLException e) {
			e.printStackTrace();
		}
		
		return myList;
		
		
		
	}
	
	
	

}

