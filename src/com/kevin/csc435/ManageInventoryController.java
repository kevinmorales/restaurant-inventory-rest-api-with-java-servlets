package com.kevin.csc435;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.stream.Collectors;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@SuppressWarnings("serial")
public class ManageInventoryController extends HttpServlet {
	
	//class constants
	final String POST_URL = "/PostInventory";
	final String GET_URL = "/GetInventory";
	final String PUT_URL = "/PutInventory";
	final String DELETE_URL = "/DeleteInventory";
	final String VIEW_URL = "/View";
	
	//class variables
	private Gson gson = new GsonBuilder().setPrettyPrinting().create();
	
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//local constants
		
		//local variables
		HttpSession session = request.getSession(false);
		ServletContext context;
		RequestDispatcher rd;
		String month;
		String id;
		
		/**********************/
		
		if (session != null)
			
			session.invalidate();
		
		//END IF
		
		session = request.getSession();
			
		String[] resources = request.getRequestURI().substring(request.getContextPath().length()).split("/");
		
		//IF the URL is ManageInventory/month/id
		if(resources.length > 3) {
			
			id = resources[3];
			month = resources[2];
			
			session.setAttribute("month", month);
	        session.setAttribute("id", id);
		}
		
		//ELSE IF the URL is ManageInventory/month
		else if(resources.length > 2) {
						
			month = resources[2];
	        session.setAttribute("month", month);
		}
		
        context = getServletContext();
        rd = context.getRequestDispatcher(GET_URL);
        rd.forward(request,response);
		
	}//END doGet
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//local constants
		
		//local variables
		HashMap<String,String> jsonMap;
		HttpSession session = request.getSession(false);
		ServletContext context;
		RequestDispatcher rd;
		String[] resources;
		String month;
		String jsonReq; 
		
		/********************************/
		
		if (session != null)
			
			session.invalidate();
		
		//END IF
		
		//Get the json request as a string
		jsonReq = request.getReader().lines().collect(Collectors.joining());
		
		jsonMap = parseJson(jsonReq);
		
		resources = request.getRequestURI().split("/");
		
		month = resources[resources.length - 1];
		
		session = request.getSession();
        session.setAttribute("month", month);
        session.setAttribute("item", jsonMap.get("item"));
        session.setAttribute("unit", jsonMap.get("unit"));
        session.setAttribute("unitCost", jsonMap.get("unitCost"));
        session.setAttribute("amount", jsonMap.get("amount"));
        
        context = getServletContext();
        rd = context.getRequestDispatcher(POST_URL);
        rd.forward(request,response);
	
	}//END doPost
	
	@Override
	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HashMap<String,String> jsonMap;
		HttpSession session = request.getSession(false);
		ServletContext context;
		RequestDispatcher rd;
		String month;
		String id;
		String jsonReq;
		
		/********************************/
		
		if (session != null)
			
			session.invalidate();
		
		//END IF
		
		//Get the json request as a string
		jsonReq = request.getReader().lines().collect(Collectors.joining());
		
		jsonMap = parseJson(jsonReq);
		
		String[] resources = request.getRequestURI().split("/");
		
		month = resources[resources.length - 2];
		id = resources[resources.length - 1];
		
		session = request.getSession();
        session.setAttribute("month", month);
        session.setAttribute("id", id);
        session.setAttribute("item", jsonMap.get("item"));
        session.setAttribute("unit", jsonMap.get("unit"));
        session.setAttribute("unitCost", jsonMap.get("unitCost"));
        session.setAttribute("amount", jsonMap.get("amount"));
        
        context = getServletContext();
        rd = context.getRequestDispatcher(PUT_URL);
        rd.forward(request,response);
		
	}//END doPut
	
	@Override
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//local constants
		
		//local variables
		HttpSession session = request.getSession(false);
		ServletContext context;
		RequestDispatcher rd;
		String month;
		int id;
		boolean successOfQuery;
		
		/**********************/
				
		if (session != null)
			
			session.invalidate();
		
		//END IF
		
		session = request.getSession();
		
		String[] resources = request.getRequestURI().substring(request.getContextPath().length()).split("/");
		
		month = resources[resources.length - 2];
		id = Integer.valueOf(resources[resources.length - 1]);
		
		successOfQuery = deleteFromDb(month, id);
		
		if(successOfQuery) {
			
			response.setStatus(HttpServletResponse.SC_OK);
			
		}
		else {
			
			response.sendError(400, "Bad Request, Invalid Parameters");
			
		}
		
	}//END doDelete
	
	public boolean deleteFromDb(String month, int id) {
		
		//local constants
		
		//local variables
		Connection con;
		ResultSet rs;
		boolean success = false;
		Statement statement;
		String query;
		
		
		/********************/
		
		try {
			
	        con = DatabaseConnection.initializeDatabase();
	        
	        statement = con.createStatement();
	        
	        query = "SELECT * FROM " + month + " WHERE id = " + id + ";";
	        
	        rs = statement.executeQuery(query);
	        
	        if (!rs.isBeforeFirst() ) {    
	            
	        	success = false;
	        	
	        	// Close DB connection
		        con.close();
	        } 
	        else {
	        
		        statement = con.createStatement();
		
		        // Add a new entry
		        query = 
		        "DELETE FROM " 	+ month	+ " " +
		        "WHERE id = " 	+ id 	+ ";" ;
		        	       
		        // Execute the Query
		        statement.executeUpdate(query);
		        
		        // Close DB connection
		        con.close();
		        
		        success = true;
	        } 
	        
		}
		catch (ClassNotFoundException e) {
			e.printStackTrace();
		} 
		catch (SQLException e) {
			e.printStackTrace();
		}
		
		return success;
				
	}//END editDb
	
	public HashMap<String,String> parseJson(String json){
		
		//local constants
		
		//local variables
		HashMap<String,String> myMap = new HashMap<String,String>();
		String[] jsonSplitArray;
		
		/************************/
		
		//Remove backticks and brackets from json
		json = json.substring(2, json.length() - 2);		
		
		//Split on the commas
		jsonSplitArray = json.split(",");
		
		for(int i = 0;i<jsonSplitArray.length;i++)
			
			myMap.put(jsonSplitArray[i].split(":")[0],jsonSplitArray[i].split(":")[1]);
		
		//END FOR
		
		return myMap;
		
	}

}//END ManageInventoryServlet
