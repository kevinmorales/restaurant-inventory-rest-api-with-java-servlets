package com.kevin.csc435;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@SuppressWarnings("serial")
public class PutInventoryController extends HttpServlet {
	
	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//local constants
		
		//local variables
		HttpSession session = request.getSession();
		PrintWriter out;
		String item, unit, month;
		String id;
		double unitCost, amount;
		boolean successOfQuery;
		
		/***********************/
		
		response.setContentType("application/json; charset=UTF-8");
		
		out = response.getWriter();
		
		if (session.getAttribute("month") != null && session.getAttribute("id") != null) {
			
			month = (String) session.getAttribute("month");
        	id = (String) session.getAttribute("id");
		
			item = (String) session.getAttribute("item");
			unit = (String) session.getAttribute("unit");
			unitCost = Double.valueOf((String) session.getAttribute("unitCost"));
			amount = Double.valueOf((String) session.getAttribute("amount"));
			
			Product myProduct = new Product(item, unit, unitCost, amount);
			
			successOfQuery = editDb(myProduct, month, id);
			
			if(successOfQuery){
				
				response.setStatus(HttpServletResponse.SC_OK);
				
			}
			else {
				
				response.sendError(HttpServletResponse.SC_BAD_REQUEST);
			}
			
		}
		else {
			out.print("{ \"success\": false }");
            out.flush();
        }   
        out.close();
		
	}
	
	public boolean editDb(Product x, String month, String id) {
		
		//local constants
		
		//local variables
		Connection con;
		boolean success = false;
		
		/********************/
		
		try {
			
	        con = DatabaseConnection.initializeDatabase();
	        
	        // Sends queries to the DB for results
	        Statement statement = con.createStatement();
	        
	        // Add a new entry
	        String query = 
	        "UPDATE " 			+ month 			+ " " +
	        "SET name = '" 		+ x.getItem() 		+ "'," +
	        "unit = '" 			+ x.getUnit() 		+ "'," +
	        "unitCost = " 		+ x.getUnitCost() 	+ "," +
	        "amountOnHand = " 	+ x.getAmount() 	+ "," +
	        "totalCost = " 		+ x.getTotalCost() 	+ " " +
	        "WHERE id = " 		+ id 				+ ";" ;
	       
	        // Execute the Query
	        statement.executeUpdate(query);
	        
	        // Close DB connection
	        con.close();
	        
	        success = true;
		} 
		catch (ClassNotFoundException e) {
			e.printStackTrace();
		} 
		catch (SQLException e) {
			e.printStackTrace();
		}
		
		return success;
				
	}//END editDb


	
		
}
